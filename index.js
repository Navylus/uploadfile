const { createServer } = require('https')
const { readFileSync } = require('fs')
const express = require('express')
const helmet = require('helmet')
const bodyParser = require('body-parser')
const session = require('express-session')
const crypto = require('crypto')
const csrf = require('csurf')
const cookieParser = require('cookie-parser')
const { homePage } = require('./home_page')
const fs = require('fs')
const path = require('path')
const fileUploader = require('express-fileupload')
const uuidv1 = require('uuid/v1')
const auth = require('basic-auth')

const app = express()
const SESSION_SECRET =
  process.env.SESSION_SECRET || crypto.randomBytes(64).toString('hex')
const creds = {
  name: '',
  pass: 'test'
}
app.use(
  helmet.contentSecurityPolicy({
    directives: {
      imgSrc: ['*'],
      upgradeInsecureRequests: true
    }
  })
)
app.use(
  session({
    name: 'SSID',
    secret: SESSION_SECRET,
    resave: true,
    saveUninitialized: true,
    cookie: { secure: true, httpOnly: true, domain: 'localhost', path: '/' }
  })
)
app.use(
  fileUploader({
    limits: { fileSize: 4 * 1000 * 1000 }
  })
)
app.use(helmet.frameguard({ action: 'deny' }))
app.use(helmet.noSniff())
app.use(
  helmet.hsts({ maxAge: 31536000, includeSubDomains: true, preload: true })
)
app.use(helmet.ieNoOpen())
app.use(helmet.referrerPolicy({ policy: 'no-referrer' }))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cookieParser())
app.use(csrf({ cookie: true }))
app.use(function(err, req, res, next) {
  console.error(err.stack)
  res.status(500).send('Something broke!')
})
app.use(function(err, req, res, next) {
  if (!req.secure) {
    res.status(500).send('Protocol not secure')
  }
})
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  next()
})

app.get('/', function(req, res) {
  res.send(
    homePage({
      csrfToken: req.csrfToken()
    })
  )
})

app.post('/fileupload', function(req, res) {
  try {
    let filename = uuidv1()
    switch (req.files.file.mimetype) {
      case 'image/jpeg':
        // Use the mv() method to place the file somewhere on your server
        req.files.file.mv('./image/' + filename + '.jpg', function(err) {
          if (err) console.log(err)
        })
        filename += '.jpg'
        break
      case 'image/jpg':
        // Use the mv() method to place the file somewhere on your server
        req.files.file.mv('./image/' + filename + '.jpg', function(err) {
          if (err) console.log(err)
        })
        filename += '.jpg'
        break
      case 'image/png':
        // Use the mv() method to place the file somewhere on your server
        req.files.file.mv('./image/' + filename + '.png', function(err) {
          if (err) console.log(err)
        })
        filename += '.png'
        break
      default:
        console.log('data not accepted')
    }
    fs.chmodSync(`${__dirname}/image/${filename}`, '666')
  } catch (err) {
    console.log(err)
  }
  res.redirect('/')
})

app.get(`/images`, function(req, res) {
  if (!auth(req)) {
    res.set('WWW-Authenticate', 'Basic realm="image access"')
    return res.status(401).send()
  }
  let { name, pass } = auth(req)
  name = escape(name)
  pass = escape(pass)
  if (name === process.env.USER && pass === process.env.PASS) {
    const pathImage = `${__dirname}/image/${req.query.image}`
    fs.readFile(pathImage, function(err, data) {
      if (err) throw err
      res.header('Content-Type', 'image/jpg')
      res.send(data)
    })
  } else {
    return res.status(401).send('bad creds')
  }
})

createServer(
  {
    key: readFileSync(process.env.SSL_KEY),
    cert: readFileSync(process.env.SSL_CERT)
  },
  app
).listen(process.env.PORT)
