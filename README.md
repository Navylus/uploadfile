start project

- clone repository
- npm install
- mkcert localhost
- mkcert -install
- cp .env.sample .env
- source .env
- node index.js

.env.sample format : {

- export PORT={yourPort}
- export SSL_KEY={certificat key}
- export SSL_CERT={ certificat }
- export USER={username}
- export PASS={password}
  }

run on : https://localhost:{yourPort}
find an image : https://localhost:{yourPort}/images?image={imageName}

creds to find an image :

- username = toto
- password = foo
