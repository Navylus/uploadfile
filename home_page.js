module.exports.homePage = function({ csrfToken }) {
  return `
  <form action="/fileupload" method="post" enctype="multipart/form-data">
    <input type="file" name="file"><br>
    <input type="hidden" name="_csrf" value="${csrfToken}">
    <input type="submit">
  </form>
  `
}
